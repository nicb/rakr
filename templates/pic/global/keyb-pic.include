.PS
#
# Usage:
#	  keyboard(k1);
#	  notequote(k1,E0,"","Door A",1);
#	  noterange(k1,Ap2,Ep4,"Water water",1,0.4);
#
octnum=0
keyrad=0.01 # this requires the -Tps driver to work
sboxht=0.3
wkeywidth=0.13
wkeyheight=0.79
bkeywidth=0.0625
bkeyheight=0.47
bkeyoff=0.03
define octave {
	C$1_$3: box with .sw at $2.se width wkeywidth \
		height wkeyheight rad keyrad thickness 1
  CS$1_$3: box invis with .nw at C$1_$3.n + (bkeyoff,0) \
    width bkeywidth ht wkeyheight
	CS$1_$3_key: box fill 1.00 with .n at CS$1_$3.n \
		width bkeywidth \
		height bkeyheight rad keyrad thickness 1
	D$1_$3: box with .sw at C$1_$3.se width wkeywidth \
		height wkeyheight rad keyrad thickness 1
  DS$1_$3: box invis with .nw at D$1_$3.n + (bkeyoff,0) \
    width bkeywidth ht wkeyheight
	DS$1_$3_key: box fill 1.00 with .n at DS$1_$3.n \
		width bkeywidth \
		height bkeyheight rad keyrad thickness 1
	E$1_$3: box with .sw at D$1_$3.se width wkeywidth \
		height wkeyheight rad keyrad thickness 1
	F$1_$3: box with .sw at E$1_$3.se width wkeywidth \
		height wkeyheight rad keyrad thickness 1
  FS$1_$3: box invis with .nw at F$1_$3.n + (bkeyoff,0) \
    width bkeywidth ht wkeyheight
	FS$1_$3_key: box fill 1.00 with .n at FS$1_$3.n \
		width bkeywidth \
		height bkeyheight rad keyrad thickness 1
	G$1_$3: box with .sw at F$1_$3.se width wkeywidth \
		height wkeyheight rad keyrad thickness 1
  GS$1_$3: box invis with .nw at G$1_$3.n + (bkeyoff,0) \
    width bkeywidth ht wkeyheight
	GS$1_$3_key: box fill 1.00 with .n at GS$1_$3.n \
		width bkeywidth \
		height bkeyheight rad keyrad thickness 1
	A$1_$3: box with .sw at G$1_$3.se width wkeywidth \
		height wkeyheight rad keyrad thickness 1
  AS$1_$3: box invis with .nw at A$1_$3.n + (bkeyoff,0) \
    width bkeywidth ht wkeyheight
	AS$1_$3_key: box fill 1.00 with .n at AS$1_$3.n \
		width bkeywidth \
		height bkeyheight rad keyrad thickness 1
	B$1_$3: box with .sw at A$1_$3.se width wkeywidth \
		height wkeyheight rad keyrad thickness 1
}

define thirdoct {
	A$1_$3: box with .sw at $2.se width wkeywidth \
		height wkeyheight rad keyrad thickness 1
	AS$1_$3: box fill 1.00 with .nw at A$1_$3.n + (bkeyoff,0)\
		width bkeywidth \
		height bkeyheight rad keyrad thickness 1
	B$1_$3: box with .sw at A$1_$3.se width wkeywidth \
		height wkeyheight rad keyrad thickness 1
}

keyboard_y_offset=0

define keyboard {

	OST__$1: box invis width 0.01 with .nw at $2.sw + (0, -$2.ht/3)
 	thirdoct(m1,OST__$1,$1)
	octave(p0,Bm1_$1,$1)
	octave(p1,Bp0_$1,$1)
	octave(p2,Bp1_$1,$1)
	octave(p3,Bp2_$1,$1)
	octave(p4,Bp3_$1,$1)
	octave(p5,Bp4_$1,$1)
	octave(p6,Bp5_$1,$1)

  C3ref_$1: box invis ht sboxht with .s at Cp3_$1.s + (0,0.025) "C\d3\u"
}

#
#
#
nqn=0
nqelev=0
define notequote_plumbing {
	nqelev := $6*0.2;
	Nq_$1: box invis ht sboxht with .s at $3_$2.n + (0,nqelev)
  line invis from Nq_$1.n to Nq_$1.s $4 $5 aligned
	line from $3_$2.n to Nq_$1.s
}
#
# notequote(keyboard,note,string1,string2,elev)
#
define notequote {
	notequote_plumbing(nqn,$1,$2,$3,$4,$5);
	nqn=nqn+1;
}
#
#
#
define lowernotequote_plumbing {
	nqelev := 0.1; # now we use constant elevation and verticalized strings
  leeway = 1.6;
	Nq_$1: box invis wid wkeywidth ht sboxht*2 with .n at $3_$2.s + (0, -nqelev)
  # the following lines require the -Tps driver to work
  if ($4 == "") then { line invis from Nq_$1.n + (0, nqelev*leeway) to Nq_$1.s + (0, nqelev*leeway) $5 ljust aligned; }\
  else { line invis from Nq_$1.n + (0,nqelev*leeway) to Nq_$1.s + (0, nqelev*leeway) $4 $5 ljust aligned; }
	line from $3_$2.s to Nq_$1.n + (0, -nqelev)
}
#
# lowernotequote(keyboard,note,string1,string2,elev)
#
define lowernotequote {
	lowernotequote_plumbing(nqn,$1,$2,$3,$4,$5);
	nqn=nqn+1;
}
#
#
#
nrn=0
xpos=0
xsize=0
xrange=0
xrsize=0
xrspace=0
ydelta=0
arrowhead=1
arrowwid=0.01
arrowht=0.01
define noterange_plumbing {
	xrspace := $7;
	nqelev := 0.35;
	xrange := $4_$2.c.x-$3_$2.c.x;
	xpos := (xrange/2)+$3_$2.c.x;
  leeway = 1.6;
	Nrstring_$1: line invis from  (xpos, nqelev*leeway) to (xpos, $3_$2.n.y+nqelev) $5 ljust aligned;
	Start_$3_$2: line from $3_$2.n to $3_$2.n + (0, 0.1)
	End_$4_$2: line from $4_$2.n to $4_$2.n + (0, 0.1)
  line <-> from Start_$3_$2.x,Start_$3_$2.n.y to End_$4_$2.x,End_$4_$2.n.y
}
#
# noterange(keyboard,notestart,notend,string1,elev,textsize)
#
define noterange {
	noterange_plumbing(nrn,$1,$2,$3,$4,$5,$6);
	nrn=nrn+1;
}
.PE
