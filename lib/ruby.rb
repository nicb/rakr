
%w(
  numeric
).each { |f| require_relative File.join('ruby', 'extensions', f) }
