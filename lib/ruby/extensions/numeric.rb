class Numeric

  #
  # +to_inch+ assumes that the value is in metric (centimeter)
  # measure unit and transforms it in inches
  #
  ONE_INCH = 2.54
  def to_inch
    self.to_f / ONE_INCH 
  end

  #
  # +to_cm+ assumes that the value is in imperial (inch)
  # measure unit and transforms it in centimeters
  #
  def to_cm
    self.to_f * ONE_INCH 
  end

end
