# frozen_string_literal: true

module Rakr

  CONFIGURATION_PATH = File.expand_path(File.join(['..'] * 3, 'conf'), __FILE__)

end
