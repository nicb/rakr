# frozen_string_literal: true

module Rakr
  module AKP

    PATH = 'akp'
    AKP_FORMAT_SPEC = File.join(Rakr::CONFIGURATION_PATH, 'akp.yaml')

  end
end

%w(
  key_group
  file
).each { |f| require_relative File.join(Rakr::AKP::PATH, f) }
