# frozen_string_literal: true

module Rakr
  module Common

    PATH = 'common'

  end
end

%w(
  configuration
  parameter
  parameter_group
).each { |f| require_relative File.join(Rakr::Common::PATH, f) }

