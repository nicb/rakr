# frozen_string_literal: true

module Rakr
  module Common

    class ParameterGroup

      attr_reader :name, :parameters, :desc

      def initialize(name, conf, data_chunk)
        @name = name
        @parameters = setup(conf, data_chunk)
      end

      def valid?
        log = ''
        valid = true
        self.parameters.each do
          |p|
          res = p.valid?
          log += (p.inspect + "\n") unless res
          valid = res if valid
        end
        [valid, log]
      end

      def create_pic_drawing(d, row, col, name)
        (res, ref) = header(d, row, col, name)
        par_no = 0
        self.parameters.each do
          |p|
          par_col = (par_no / d.conf.num_par_rows.to_f).floor
          par_row = par_no % d.conf.num_par_rows.to_f
          res += p.create_pic_drawing(d, ref, par_no, par_row, par_col)
          par_no += 1
        end
        res
      end

      def lookup(par_name)
        res = nil
        self.parameters.each do
          |p|
          if p.name == par_name
            res = p
            break
          end
        end
        res
      end

    protected

      def setup(conf, chunk)
        res = []
        @desc = conf.has_key?('desc') ? conf.delete('desc') : ''
        conf.each do
          |k, par|
          name = k
          off = par['offset']
          rng = par['range']
          desc = par.has_key?('desc') ? par['desc'] : ''
          size = par.has_key?('size') ? par['size'] : 1
          data_offset = par.has_key?('data_offset') ? par['data_offset'] : 0
          cls = par.has_key?('class') ? eval(par['class']) : nil
          cls = (rng[0] < 0 || rng[1] < 0 ? SignedParameter : UnsignedParameter) if cls.nil?
          pg = cls.new(name, off, rng, size, desc, data_offset)
          pg.value = chunk.data
          res << pg
        end
        res
      end

    private

      PIC_SUBDIR = 'parameter_group'

      def header(d, row, col, name)
        pgpicname = "%s%02d%02d" % [ name.gsub(/\s/,'_'), row, col ]
        res = Rakr::Pic::Template.read(File.join(PIC_SUBDIR, 'header.pic.erb'), binding)
        [res, pgpicname]
      end

    end

  end
end
