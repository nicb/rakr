module Rakr
  module Common
    #
    # +Rakr::Common::Configuration+
    #
    # is a +Hash+ whose values are are all
    # +HashElement+s, to allow multiple hash keys
    # with enumeration output through the +next+ method
    #
    class Configuration < Hash

      class Element
      
        attr_reader :value, :index

        def initialize(value)
          @index = 0
          @value = setup(value)
        end

        def value=(value)
          @value << value
        end

        def next
          res = @value[@index]
          @index += 1
          res
        end

        def next_in_loop
          res = self.next
          res = (self.reset && self.next) if res.nil?
          res
        end

        def size
          self.value.size
        end

        def reset
          @index = 0
        end

      private

        def setup(value)
          res = case 
            when value.kind_of?(Array) then value
            else [ value ]
          end
          res
        end

      end

      def initialize(hash = {})
        setup(hash)
      end

      alias_method :old_store, :store

      def []=(key, value)
        if self.has_key?(key)
          self[key].value = value
        else
          self.old_store(key, Element.new(value))
        end
      end

      alias_method :old_fetch, :fetch

      def reset
        self.values.each { |v| v.reset }
      end

    private

      def setup(hash)
        hash.each { |k, v| self[k] = v }
      end
      
    end

  end
end
