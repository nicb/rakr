# frozen_string_literal: true

module Rakr
  module Common

    class Parameter

      attr_reader :name, :offset, :allowed_range, :desc, :size, :format, :data_offset

      def initialize(name, offset, range, size = 1, format = 'C*', desc = '', doff = 0)
        @name = name
        @offset = offset
        @allowed_range = range
        @size = size
        @format = format
        @desc = desc
        @data_offset = doff
      end

      def inspect
        doff = self.data_offset == 0 ? '' : (", data offset: %+d" % [self.data_offset])
        desc = self.desc.empty? ? '' : ", #{self.desc}"
        "#{self.class.name}: \"#{self.name}\" value: #{self.value.to_s} (allowed_range: #{self.allowed_range.to_s}#{doff}#{desc})"
      end

      def value=(datastream, alternate_format = nil)
        fmt = alternate_format || self.format
        res = at(datastream, self.offset, self.offset + self.size - 1, fmt)
        @value = res
      end

      def value
        raise ValueNotSet unless @value
        res = @value.map { |v| v + self.data_offset }
        res = self.size == 1 ? res.first : res
        res
      end

      def valid?
        res = false
        if self.size == 1
          res = each_valid?(self.value)
        else
          self.value.each do
            |v|
            res = each_valid?(v)
            break unless res
          end
        end
        res
      end

      def create_pic_drawing(d, ref, par_no, par_row, par_col)
        core(d, ref, par_no, par_row, par_col)
      end

    protected

      def at(data, start, finish, format)
        finish = finish ? finish : start
        data[start..finish].unpack(format)
      end

      def each_valid?(v)
        v >= self.allowed_range[0] && v <= self.allowed_range[1]
      end

    private
    
      PIC_SUBDIR = 'parameter'

      def core(d, ref, par_no, par_row, par_col)
        val = self.value.kind_of?(Array) ? self.value.to_s : self.value
        format = case
          when val.kind_of?(Float) then "%4.1f"
          when val.kind_of?(String) then "%s"
          else "%4d"
        end
        short_name = shorten_name(d)
        parameter_label = "%-s" % [short_name]
        parameter_value = format  % [self.value]
        Rakr::Pic::Template.read(File.join(PIC_SUBDIR, 'core.pic.erb'), binding)
      end

      def shorten_name(d)
        max = d.conf.max_par_name_length 
        return self.name if self.name.size <= d.conf.max_par_name_length
        binder = d.conf.par_name_binder
        side_size = (max-binder.size)/2.0
        left_side = side_size.ceil
        right_side = side_size.floor
        self.name[0..left_side-1] + binder + self.name[-right_side..-1]
      end

    end

    class UnsignedParameter < Parameter

      def initialize(name, offset, range, size = 1, desc = '', doff = 0)
        super(name, offset, range, size, 'C*', desc, doff)
      end

    end

    class SignedParameter < Parameter

      def initialize(name, offset, range, size = 1, desc = '', doff = 0)
        super(name, offset, range, size, 'c*', desc, doff)
      end

    end

    class StringParameter < Parameter

      def initialize(name, offset, range = [0, 127], size = 1, desc = '', data_offset = 0)
        # there can be no data_offset, so we skip the argument
        super(name, offset, range, size, 'A*', desc)
      end

      def value=(datastream)
        first_char = super(datastream, 'c').first
        if first_char == 0
          @value = ""
          @size = 0
        else
          @value = super(datastream).first
          @size = @value.size
        end
      end

      def value
        raise ValueNotSet unless @value
        @value
      end

      def valid?
        res = true
        self.value.bytes.each do
          |b|
          res = each_valid?(b)
          break unless res
        end
        res
      end

      def inspect
        desc = self.desc.empty? ? '' : ", (#{self.desc})"
        "#{self.class.name}: \"#{self.name}\" value: \"#{self.value.to_s}\"#{desc}"
      end

    end

  end
end
