# frozen_string_literal: true

require 'ruby-mext'
require 'git/describe'

module Rakr
  module Pic

    class Drawing

      attr_reader :file, :name, :conf
      attr_accessor :last_ref

      def initialize(filename, cls = Rakr::Riff::File)
        @name = filename
        @file = cls.new(self.name)
        @conf = MethodedHash.create(YAML.load(File.open(File.join(Rakr::CONFIGURATION_PATH, 'pic_configuration.yaml'), 'r')))
      end

      def output
        res = header
        res += self.file.create_pic_drawing(self)
        res += trailer
        res
      end

    private

      PIC_SUBDIR = 'global'

      def header
        Template.read(File.join(PIC_SUBDIR, 'header.pic.erb'), binding)
      end

      def trailer
        Template.read(File.join(PIC_SUBDIR, 'trailer.pic.erb'), binding)
      end

    end

  end
end
