# frozen_string_literal: true

require 'erb'

module Rakr
  module Pic
    module Template
	
      TEMPLATE_PATH = File.expand_path(File.join(['..'] * 4, 'templates', 'pic'), __FILE__)

	    class << self
	
	      def read(template_file, bindings)
	        tmplate = nil
	        File.open(File.join(TEMPLATE_PATH, template_file), 'r') do
	          |fh|
	          tmplate = fh.readlines.join
	        end
	        res = ERB.new(tmplate, trim_mode: '-')
	        res.result(bindings)
	      end
	
	    end
    end

  end
end
