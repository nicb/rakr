# frozen_string_literal: true

require 'yaml'

module Rakr
  module AKP

    class File < Riff::File

      attr_reader :parameter_groups

      DEFAULT_AKP_STREAM_READING_OFFSET = 0x0C

      def initialize(fname)
        super(fname, DEFAULT_AKP_STREAM_READING_OFFSET)
        @parameter_groups = {}
        setup
      end

      NUMBER_OF_KEYGROUPS_PARAMETER_INDEX = 1

      def number_of_keygroups
        self.parameter_groups['prg '].first.parameters[NUMBER_OF_KEYGROUPS_PARAMETER_INDEX].value
      end

      def create_pic_drawing(d)
        res = ''
        idx = 0
        self.parameter_groups.each do
          |k, pga|
          iidx = 0
          pga.each do
            |pg|
            name = "%s %02d" % [k, iidx]
            row = (idx/d.conf.num_cols).to_i
            col = idx % d.conf.num_cols
            res += pg.create_pic_drawing(d, row, col, name)
            idx += 1
            iidx += 1
          end
        end
        res
      end

    private

      def setup
        spec = ::Rakr::Common::Configuration.new(YAML.load(::File.open(AKP_FORMAT_SPEC, 'r')))
        kgrp_idx = 0
        self.chunks.each do
          |chunk|
          key = chunk.name
          raise ::Rakr::UnknownRiffChunk unless spec.has_key?(key)
          conf = spec[key].next_in_loop
          cls = conf.kind_of?(Hash) && conf.has_key?('class') ? eval('::Rakr::' + conf['class']) : ::Rakr::Common::ParameterGroup
          num = spec[key].size > 1 ? spec[key].index : nil
          num = key == "kgrp" ? kgrp_idx : num
          add_parameter(cls, key, conf, chunk, num)
          kgrp_idx +=1 if key == 'kgrp'
        end
        check_validity
      end

      def add_parameter(cls, k, parconf, chunk, num = nil)
        name = num.nil? ? k : ("%s %d" % [k, num])
        pg = cls.new(name, parconf, chunk)
        self.parameter_groups[k] = [] unless self.parameter_groups.has_key?(k)
        self.parameter_groups[k] << pg
      end

      def check_validity
        self.parameter_groups.each do
          |k, pgg|
          v = true; log = ''
          pgg.each do
            |pg|
            (thisv, thislog) = pg.valid?
            unless thisv
              v = thisv
              log += thislog
            end
          end
          raise Rakr::InvalidParameterGroup, log unless v
        end
      end

    end

  end
end
