# frozen_string_literal: true

module Rakr
  module AKP

    class KeyGroup < ::Rakr::Common::ParameterGroup

      def create_pic_drawing(d, row, col, name)
        res = nil
        idx = (row * d.conf.num_par_rows) + col
        prefix = idx.odd? ? '' : 'lower'
        if self.bot_note == self.top_note
          res = "lowernotequote(k1,#{m2n(self.bot_note)},\"\",\"#{self.sample_name}\",0.4);\n"
        else
          res = "noterange(k1,#{m2n(self.bot_note)},#{m2n(self.top_note)},\"#{self.sample_name}\", #{row*0.2}, 0.4);\n"
        end
        res
      end

      def bot_note
        self.lookup('low_note').value
      end

      def top_note
        self.lookup('high_note').value
      end

      def sample_name
        self.lookup('sample_name').value
      end

    protected

      def setup(orig_conf, chunk)
        res = []
        conf = ::Rakr::Common::Configuration.new(orig_conf)
        chunk.sub_chunks.each do
          |sc|
          this_conf = conf[sc.name].next_in_loop
          res.concat(super(this_conf, sc))
        end
        res
      end

    private
    
      NOTE_NAMES = %w(A AS B C CS D DS E F FS G GS)
      LOWEST_MIDI_NOTE = 21
      LOWEST_OCTAVE = -1
      OCTAVE_SWITCH_INDEX = 3  # the octave switch is on the 4th note of the array (index: 3)

      def m2n(midi_note)
        ndist = midi_note - LOWEST_MIDI_NOTE
        note_index = ndist % 12
        oct_switch = ndist >= OCTAVE_SWITCH_INDEX ? 1 : 0
        oct_num = ((ndist / 12.0).floor) + LOWEST_OCTAVE + oct_switch
        oct_index = oct_num >= 0 ? 'p' : 'm'
        oct = "%c%d" % [oct_index, oct_num.abs]
        res = "%s%s" % [NOTE_NAMES[note_index], oct] 
        res
      end

    end

  end
end
