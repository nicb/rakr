# frozen_string_literal: true

require 'riffola'

module Rakr
  module Riff

    class File

      attr_reader :filename, :chunks, :offset

      def initialize(fname, offset = 0)
        @filename = fname
        @offset = 0
        @chunks = read_chunks(offset)
      end

      def lookup(chunk_name, subchunk_name = '')
        res = nil
        self.chunks.each do
          |c|
          if c.name == chunk_name
            res = c
            break
          end
        end
        unless subchunk_name.empty?
          res.sub_chunks.each do
            |sc|
            if sc.name == subchunk_name
              res = sc
              break
            end
          end
        end
        res
      end

      #
      # +create_pic_drawing+:
      #
      # this method is supposed to be sub-classed in order to provide the
      # proper drawing. It gets called by the +::Rakr::Pic::Drawing#output+
      # method. As it stands here, it just generates an exception.
      #
      def create_pic_drawing(d)
        raise PureVirtualMethodCalled, 'Rakr::Riff::File#create_pic_drawing'
      end

    protected

      def read_chunks(offset)
        Riffola.read(self.filename, offset: offset, warnings: false)
      end

    end

  end
end
