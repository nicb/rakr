# frozen_string_literal: true

module Rakr
  module Riff

    PATH = 'riff'

  end
end

%w(
  file
).each { |f| require_relative File.join(Rakr::Riff::PATH, f) }
