# frozen_string_literal: true

module Rakr
  module Pic

    PATH = 'pic'

  end
end

%w(
  template_read
  drawing
).each { |f| require_relative File.join(Rakr::Pic::PATH, f) }
