# frozen_string_literal: true

module Rakr

  class Error < ::StandardError
  end

  class PureVirtualMethodCalled < Error
  end

  class ValueNotSet < Error
  end

  class InvalidParameterGroup < Error
  end

  class UnknownRiffChunk < Error
  end

  class PureVirtualMethodCalled < Error
  end

end
