# frozen_string_literal: true

require_relative 'ruby'

%w(
  version
  constants
  exceptions
  pic
  common
  riff
  akp
).each { |f| require_relative File.join('rakr', f) }
