# `rakr`: a native ruby implementation of a reader for AKAI files

This is gem (will) read .AKP and .AKM files.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'rakr'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install rakr

## Usage

TODO: when it will be completed, we'll write here some documentation on how to use
this gem.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on `git.smerm.org` at https://git.smerm.org/nicb/rakr.
