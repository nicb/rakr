# frozen_string_literal: true

require_relative 'lib/rakr/version'

Gem::Specification.new do |spec|
  spec.name          = 'rakr'
  spec.version       = Rakr::VERSION
  spec.authors       = ['Nicola Bernardini']
  spec.email         = ['nicola.bernardini.rome@gmail.com']

  spec.summary       = 'A native ruby implementation of a reader for AKAI files'
  spec.description   = 'A native ruby implementation of a reader for AKAI files'
  spec.homepage      = 'https://git.smerm.org/nicb/rakr'
  spec.required_ruby_version = Gem::Requirement.new('>= 3.0.0')

  spec.metadata['allowed_push_host'] = 'https://rubygems.org'

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage
  spec.metadata["changelog_uri"] = 'https://git.smerm.org/nicb/rakr/CHANGELOG.md'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency 'byebug'
  spec.add_dependency 'riffola'
  spec.add_dependency 'ruby-mext'
  spec.add_dependency 'git-describe'

end
