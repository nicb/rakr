module Rakr
  module Doc
    class Plotter
      attr_reader :chunk

      AKAI_EXAMPLE = File.expand_path(File.join(['..'] * 2, 'spec', 'fixtures', 'akp_test.akp'), __FILE__)

      def initialize(chnk, schnk = '')
        setup(chnk, schnk)
      end

      def to_pic
        res = header
        res += data
        res += trailer
        res
      end

    private

      def setup(chnk, schnk)
        rffile = Rakr::Riff::File.new(AKAI_EXAMPLE, 12)
        @chunk = rffile.lookup('kgrp', 'zone')
      end

      PAGE_DIMENSIONS = [28, 20]
      BOX_HT = 1
      CELL_PER_LINE = 16
      DOC_ADDR_OFFSET = 0x126

      def header
        ".PS\nscale=2.54;\n" +
        "maxpswid=#{PAGE_DIMENSIONS[0]}\nmaxpsht=#{PAGE_DIMENSIONS[1]}\n" +
        "frame_wid=maxpswid*0.8\n" +
        "dbox_size=frame_wid/#{CELL_PER_LINE}\n" +
        "#{title}\n"
      end

      def trailer
        ".PF\n"
      end

      def title
        "Title: \"\\fB\\s[+8]#{self.chunk.name}\\s0\\fP\" with .n at (maxpswid/2, 0)\n"
      end

      def frame(page_no)
        frame_pos = page_no == 1 ? "Title.s" : "Addresses_#{page_no-1}.s"
        "Values_#{page_no}: box wid frame_wid ht #{BOX_HT} with .n at #{frame_pos} - (0, #{BOX_HT})\n" +
        "\"Data values\" with .e at Values_#{page_no}.w - (0.1, 0) rjust\n" +
        "Addresses_#{page_no}: box wid frame_wid ht #{BOX_HT} with .n at Values_#{page_no}.s\n" +
        "\"Addresses\" with .e at Addresses_#{page_no}.w - (0.1, 0) rjust\n"
      end

      def page(page_no)
        res = frame(page_no)
        idx=CELL_PER_LINE*(page_no-1)
        0.upto(CELL_PER_LINE-1) do
          |pidx|
          d = self.chunk.data.bytes[idx]
          printed_d = (d.chr.match?(/(\w|\s)/) && idx < 23) ? "'#{d.chr}'" : ("0x%02X" % d)
          res += ("Vcell_#{idx}: box invis wid dbox_size ht #{BOX_HT} with .w at Values_#{page_no}.w + (dbox_size*%d, 0)  \"#{printed_d}\"\n" % pidx)
          res += "line from Vcell_#{idx}.ne to Vcell_#{idx}.se\n"
          res += ("Acell_#{idx}: box invis wid dbox_size ht #{BOX_HT} with .w at Addresses_#{page_no}.w + (dbox_size*%d, 0)  \"0x%02X\" \"0x%03X\"\n" % [pidx, idx, DOC_ADDR_OFFSET + idx])
          res += "line from Acell_#{idx}.ne to Acell_#{idx}.se\n"
          idx += 1
        end
        res
      end

      def data
        res = ''
        n_of_pages = self.chunk.size / CELL_PER_LINE
        1.upto(n_of_pages) { |n| res += page(n) }
        res
      end

    end
  end
end
