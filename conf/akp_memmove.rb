
correction = ARGV[0].to_i(10) || 4

lines = STDIN.readlines

lines.each do
  |l|
  if l.match?(/^# [0-9A-F]+{3}h:/)
    (addr, rest) = l.split(/\t/, 2)
    addr.sub!(/^# ([0-9A-F]+{3})h.*$/, '\1')
    new_addr = addr.to_i(16) + correction
    printf("# %04xh:\t%s", new_addr, rest)
  else
    print l
  end
end
