# frozen_string_literal: true

RSpec.describe Numeric do

  before :example do
    @new_methods = [:to_inch, :to_cm]
    @a_numeric = 2.54
    @should_be = 1.0
  end

  it 'responds to the new methods' do
    @new_methods.each { |m| expect(@a_numeric.respond_to?(m)).to be true }
  end

  it 'has new methods that work' do
    expect(@a_numeric.to_inch).to eq(@should_be)
    expect(@should_be.to_cm).to eq(@a_numeric)
    expect(@a_numeric.to_inch.to_cm).to eq(@a_numeric)
  end
end
