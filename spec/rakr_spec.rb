# frozen_string_literal: true

RSpec.describe Rakr do
  it "has a version number" do
    expect(Rakr::VERSION).not_to be nil
  end
end
