# frozen_string_literal: true

RSpec.describe Rakr::Common::Parameter do
  
  before :example do
    @name = 'test'
    @offset = 0
    @size = 4
    @desc = 'test parameter'
    @data = "\xff\xfe\xfd\xfc"
    @unsigned_range = [0, 255]
    @signed_range = [-256, 255]
    @unsigned_data_should_be = [255, 254, 253, 252]
    @signed_data_should_be = [-1, -2, -3, -4]
    @data_offset = +25
    @string_data = "\x08\x53\x41\x4D\x50\x4C\x45\x20\x31\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
    @string_offset = 1
    @string_value = "SAMPLE 1"
    @string_size = 20
    @string_range = [0, 125]
    @long_string_tester =  "this_is_a_very_very_long_parameter_string"
    @short_string_should_be = "this_...ring"
    @a_12_char_string = "this_has_12c"
    @smaller_string = "123"
  end
  
  it 'can be created' do
    expect(Rakr::Common::Parameter.new(@name, @offset, @unsigned_range)).not_to be nil
    expect(Rakr::Common::UnsignedParameter.new(@name, @offset, @unsigned_range)).not_to be nil
    expect(Rakr::Common::SignedParameter.new(@name, @offset, @signed_range)).not_to be nil
    expect(Rakr::Common::Parameter.new(@name, @offset, @unsigned_range, @size)).not_to be nil
    expect(Rakr::Common::UnsignedParameter.new(@name, @offset, @unsigned_range, @size)).not_to be nil
    expect(Rakr::Common::SignedParameter.new(@name, @offset, @signed_range, @size)).not_to be nil
    expect(Rakr::Common::Parameter.new(@name, @offset, @unsigned_range, @size, @desc)).not_to be nil
    expect(Rakr::Common::UnsignedParameter.new(@name, @offset, @unsigned_range, @size, @desc)).not_to be nil
    expect(Rakr::Common::SignedParameter.new(@name, @offset, @signed_range, @size, @desc)).not_to be nil
    expect(Rakr::Common::Parameter.new(@name, @offset, @unsigned_range, @size, @desc, @data_offset)).not_to be nil
    expect(Rakr::Common::UnsignedParameter.new(@name, @offset, @unsigned_range, @size, @desc, @data_offset)).not_to be nil
    expect(Rakr::Common::SignedParameter.new(@name, @offset, @signed_range, @size, @desc, @data_offset)).not_to be nil
    expect(Rakr::Common::StringParameter.new(@name, @string_offset, @string_range, @string_size)).not_to be nil
  end

  it 'sets the value appropriately' do
    # unsigned
    up = Rakr::Common::UnsignedParameter.new(@name, @offset, @unsigned_range)
    expect(up.value = @data).to eq(@data)
    expect(up.value).to eq(@data.unpack("C").first)
    # signed
    sp = Rakr::Common::SignedParameter.new(@name, @offset, @unsigned_range)
    expect(sp.value = @data).to eq(@data)
    expect(sp.value).to eq(@data.unpack("c").first)
    # unsigned, 4 bytes
    up = Rakr::Common::UnsignedParameter.new(@name, @offset, @unsigned_range, @size)
    expect(up.value = (@data)).to eq(@data)
    expect(up.value).to eq(@data.unpack("C4"))
    # signed, 4 bytes
    sp = Rakr::Common::SignedParameter.new(@name, @offset, @unsigned_range, @size)
    expect(sp.value = (@data)).to eq(@data)
    expect(sp.value).to eq(@data.unpack("c4"))
    #
    # with data offset
    #
    # unsigned
    up = Rakr::Common::UnsignedParameter.new(@name, @offset, @unsigned_range, 1, @desc, @data_offset)
    expect(up.value = @data).to eq(@data)
    expect(up.value).to eq(@data.unpack("C").first + @data_offset)
    # signed
    sp = Rakr::Common::SignedParameter.new(@name, @offset, @unsigned_range, 1, @desc, @data_offset)
    expect(sp.value = @data).to eq(@data)
    # unsigned, 4 bytes
    up = Rakr::Common::UnsignedParameter.new(@name, @offset, @unsigned_range, @size, @desc, @data_offset)
    expect(up.value = @data).to eq(@data)
    expect(up.value).to eq(@data.unpack("C4").map { |v| v  + @data_offset })
    # signed, 4 bytes
    sp = Rakr::Common::SignedParameter.new(@name, @offset, @unsigned_range, @size, @desc, @data_offset)
    expect(sp.value = (@data)).to eq(@data)
    expect(sp.value).to eq(@data.unpack("c4").map { |v| v + @data_offset })
    #
    # string parameter
    #
    sp = Rakr::Common::StringParameter.new(@name, @string_offset, @string_range, @string_size)
    expect(sp.value = (@string_data)).to eq(@string_data)
    expect(sp.value).to eq(@string_value)
    expect(sp.size).to eq(@string_value.size)
  end

  it 'has a valid? method that works' do
    # unsigned
    up = Rakr::Common::UnsignedParameter.new(@name, @offset, @unsigned_range)
    up.value = (@data)
    expect(up.valid?).to be true
    up = Rakr::Common::UnsignedParameter.new(@name, @offset, [0, 1]) # very small range
    up.value = (@data)
    expect(up.valid?).to be false
    # signed
    sp = Rakr::Common::SignedParameter.new(@name, @offset, @signed_range)
    sp.value = @data
    expect(sp.valid?).to be true
    sp = Rakr::Common::UnsignedParameter.new(@name, @offset, [10, 20]) # range outside of values
    sp.value = @data
    expect(sp.valid?).to be false
    # unsigned array
    up = Rakr::Common::UnsignedParameter.new(@name, @offset, @unsigned_range, @size)
    up.value = @data
    expect(up.valid?).to be true
    up = Rakr::Common::UnsignedParameter.new(@name, @offset, [0, 1], @size) # very small range
    up.value = @data
    expect(up.valid?).to be false
    # signed
    sp = Rakr::Common::SignedParameter.new(@name, @offset, @signed_range, @size)
    sp.value = @data
    expect(sp.valid?).to be true
    sp = Rakr::Common::UnsignedParameter.new(@name, @offset, [10, 20], @size) # range outside of values
    sp.value = @data
    expect(sp.valid?).to be false
    #
    # string parameter
    #
    sp = Rakr::Common::StringParameter.new(@name, @string_offset, @string_range, @string_size)
    sp.value = @string_data
    expect(sp.valid?).to be true
    #
    # with a non-string
    #
    sp = Rakr::Common::StringParameter.new(@name, @string_offset, @string_range, @string_size)
    sp.value = "\x04\x80\x81\xff\x83"
    expect(sp.valid?).to be false
  end

  it 'has an :inspect method that works' do
    # unsigned
    up = Rakr::Common::UnsignedParameter.new(@name, @offset, @unsigned_range)
    up.value = @data
    expect(up.inspect).to eq("Rakr::Common::UnsignedParameter: \"#{@name}\" value: #{@data.unpack('C').first} (allowed_range: #{@unsigned_range.to_s})")
    up = Rakr::Common::UnsignedParameter.new(@name, @offset, @unsigned_range, @size)
    up.value = @data
    expect(up.inspect).to eq("Rakr::Common::UnsignedParameter: \"#{@name}\" value: #{@data.unpack('C4')} (allowed_range: #{@unsigned_range.to_s})")
    up = Rakr::Common::UnsignedParameter.new(@name, @offset, @unsigned_range, @size, @desc)
    up.value = @data
    expect(up.inspect).to eq("Rakr::Common::UnsignedParameter: \"#{@name}\" value: #{@data.unpack('C4')} (allowed_range: #{@unsigned_range.to_s}, #{@desc})")
    up = Rakr::Common::UnsignedParameter.new(@name, @offset, @unsigned_range, @size, @desc, @data_offset)
    up.value = @data
    doff = "%+d" % [@data_offset]
    expect(up.inspect).to eq("Rakr::Common::UnsignedParameter: \"#{@name}\" value: #{@data.unpack('C4').map { |v| v + @data_offset }} (allowed_range: #{@unsigned_range.to_s}, data offset: #{doff}, #{@desc})")
    # signed
    sp = Rakr::Common::SignedParameter.new(@name, @offset, @signed_range)
    sp.value = @data
    expect(sp.inspect).to eq("Rakr::Common::SignedParameter: \"#{@name}\" value: #{@data.unpack('c').first} (allowed_range: #{@signed_range.to_s})")
    sp = Rakr::Common::SignedParameter.new(@name, @offset, @signed_range, @size)
    sp.value = @data
    expect(sp.inspect).to eq("Rakr::Common::SignedParameter: \"#{@name}\" value: #{@data.unpack('c4')} (allowed_range: #{@signed_range.to_s})")
    sp = Rakr::Common::SignedParameter.new(@name, @offset, @signed_range, @size, @desc)
    sp.value = @data
    expect(sp.inspect).to eq("Rakr::Common::SignedParameter: \"#{@name}\" value: #{@data.unpack('c4')} (allowed_range: #{@signed_range.to_s}, #{@desc})")
    sp = Rakr::Common::SignedParameter.new(@name, @offset, @signed_range, @size, @desc, @data_offset)
    sp.value = @data
    doff = "%+d" % [@data_offset]
    expect(sp.inspect).to eq("Rakr::Common::SignedParameter: \"#{@name}\" value: #{@data.unpack('c4').map { |v| v + @data_offset }} (allowed_range: #{@signed_range.to_s}, data offset: #{doff}, #{@desc})")
    #
    # string
    #
    sp = Rakr::Common::StringParameter.new(@name, @string_offset, @string_range, @string_size)
    sp.value = @string_data
    expect(sp.inspect).to eq("Rakr::Common::StringParameter: \"#{@name}\" value: \"#{@string_value}\"")
    sp = Rakr::Common::StringParameter.new(@name, @string_offset, @string_range, @string_size, @desc)
    sp.value = @string_data
    expect(sp.inspect).to eq("Rakr::Common::StringParameter: \"#{@name}\" value: \"#{@string_value}\", (#{@desc})")
  end

  it 'has a private string shortening method that works' do
    akp_file = File.join(FIXTURES_PATH, 'akp_test.akp')
    d = Rakr::Pic::Drawing.new(akp_file, Rakr::AKP::File)
    p = Rakr::Common::Parameter.new(@long_string_tester, @offset, @unsigned_range)
    expect(p.send(:shorten_name, d)).to eq(@short_string_should_be)
    p = Rakr::Common::Parameter.new(@a_12_char_string, @offset, @unsigned_range)
    expect(p.send(:shorten_name, d)).to eq(@a_12_char_string)
    p = Rakr::Common::Parameter.new(@smaller_string, @offset, @unsigned_range)
    expect(p.send(:shorten_name, d)).to eq(@smaller_string)
  end

end
