# frozen_string_literal: true

RSpec.describe Rakr::Common::Configuration do
  
  before :example do
    @config_spec = File.join(FIXTURES_PATH, 'configuration_test.yaml')
    @some_hash = { 'A' => 0, B: 1, 'C' => 2 }
  end
  
  it 'can be created' do
    expect(Rakr::Common::Configuration.new).not_to be nil
    expect(Rakr::Common::Configuration.new(@some_hash)).not_to be nil
  end

  it 'is created properly' do
    c = Rakr::Common::Configuration.new(@some_hash)
    expect(c.keys).to eq(@some_hash.keys)
    c.each do
      |k, v| 
      expect(v.class).to eq(Rakr::Common::Configuration::Element)
      expect(v.value).to eq([@some_hash[k]])
    end
  end

  it 'works properly' do
    iterations = 10
    c = Rakr::Common::Configuration.new
    0.upto(iterations-1) { @some_hash.each { |k, v| c[k] = v } }
    c.each { |k, v| expect(v.value).to eq([@some_hash[k]] * iterations) }
  end

  it 'has a :next method that works as expected (proposing a next value)' do
    #
    # simple
    #
    values = [10, 20, 30, 40, 50]
    key = 'the_only_key'
    c = Rakr::Common::Configuration.new
    values.each { |v| c[key] = v }
    expect(c[key].value).to eq(values)
    values.each { |v| expect(c[key].next).to eq(v) }
    expect(c[key].next).to be nil
    #
    # complicated
    #
    values = [10, [20, 21, 22], 30, 40, 50]
    key = 'the_only_key'
    c = Rakr::Common::Configuration.new({ key => values })
    expect(c[key].value).to eq(values)
    values.each { |v| expect(c[key].next).to eq(v) }
    expect(c[key].next).to be nil
    #
    # even more complicated
    #
    conf = YAML.load(File.open(@config_spec, 'r'))
    results = conf.delete('results')
    c = Rakr::Common::Configuration.new(conf)
    results.each do
      |flk, value|
      class_should_be = eval(value['class'])
      expect(c[flk].class).to(eq(class_should_be), value['class'])
      expect(c[flk].value).to(eq(value['values']), "#{value['values']} != #{c[flk].value}")
      value['next'].each { |v| expect(n = c[flk].next).to(eq(v), "#{v} != #{n}") }
    end
  end

  it 'has a Configuration#reset method that works as expected' do
    #
    # here we go once
    #
    conf = YAML.load(File.open(@config_spec, 'r'))
    results = conf.delete('results')
    c = Rakr::Common::Configuration.new(conf)
    results.each do
      |flk, value|
      class_should_be = eval(value['class'])
      expect(c[flk].class).to(eq(class_should_be), value['class'])
      expect(c[flk].value).to(eq(value['values']), "#{value['values']} != #{c[flk].value}")
      value['next'].each { |v| expect(n = c[flk].next).to(eq(v), "#{v} != #{n}") }
    end
    #
    # now we reset all element counters
    #
    c.reset
    #
    # and we go once again
    #
    results.each do
      |flk, value|
      value['next'].each { |v| expect(n = c[flk].next).to(eq(v), "#{v} != #{n}") }
    end
  end

end
