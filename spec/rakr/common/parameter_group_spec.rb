# frozen_string_literal: false

RSpec.describe Rakr::Common::ParameterGroup do
  
  before :example do
    @good_akp_file = File.join(FIXTURES_PATH, 'akp_test.akp')
    @test_file = File.join(FIXTURES_PATH, 'pg_test.riff')
    @good_chunk = Rakr::Riff::File.new(@test_file).lookup('RIFF', 'good') # the 'good' chunk
    @bad_chunk  = Rakr::Riff::File.new(@test_file).lookup('RIFF', 'bad ') # the 'bad ' chunk
    @group_name = 'good'
    @unsigned_data_should_be = [127, 250, 253, 252]
    @signed_data_should_be = [127, -6, -3, -4]
    @parameters_proper_data = [ 0x7f, -6, @unsigned_data_should_be, @signed_data_should_be ]
    @group_test_name = 'test'
    @fake_conf = { @group_test_name => { 
	      'test unsigned' => { 'offset' => 0x0, 'range' => [0, 255] },
	      'test signed' => { 'offset' => 0x1, 'range' => [-128, 127] },
	      'test unsigned 4' => { 'offset' => 0x0, 'range' => [0, 255], 'size' => 4 },
	      'test signed 4' => { 'offset' => 0x0, 'range' => [-128, 127], 'size' => 4 },
      }
    }
    @fake_conf_invalid = { @group_test_name => {
        'test unsigned' => { 'offset' => 0x0, 'range' => [0, 1] },
        'test signed' => { 'offset' => 0x1, 'range' => [-128, -127] },
        'test unsigned 4' => { 'offset' => 0x0, 'range' => [0, 1], 'size' => 4 },
        'test signed 4' => { 'offset' => 0x0, 'range' => [-128, -127], 'size' => 4 },
      }
    }
    @data = "\x7f\xfa\xfd\xfc".force_encoding('ASCII')
    expect(@good_chunk.data.unpack('C*')).to eq(@data.unpack('C*'))
    expect(@bad_chunk.data.unpack('C*')).to eq(@data.unpack('C*'))
    @n_parameters_should_be = 4
  end
  
  it 'can be created' do
    expect(Rakr::Common::ParameterGroup.new(@group_name, @fake_conf[@group_test_name], @good_chunk)).not_to be nil
  end

  it 'can be created correctly' do
    pg = Rakr::Common::ParameterGroup.new(@group_name, @fake_conf[@group_test_name], @good_chunk)
    expect(pg.parameters.empty?).not_to be true
    expect(pg.parameters.size).to eq(@n_parameters_should_be)
    conf_parameter_class = [ Rakr::Common::UnsignedParameter, Rakr::Common::SignedParameter, Rakr::Common::UnsignedParameter, Rakr::Common::SignedParameter,]
    pg.parameters.each_index { |idx| expect(pg.parameters[idx].class).to eq(conf_parameter_class[idx]) }
    idx = 0
    pg.parameters.each do
      |parm|
      expect(parm.value).to eq(@parameters_proper_data[idx]), "Parameter \"#{parm.name}\" #{parm.value} != #{@parameters_proper_data[idx]}"
      expect(parm.valid?).to be true
      if idx < 2
        expect(parm.value).to be >= parm.allowed_range.first
        expect(parm.value).to be <= parm.allowed_range.last
      else
        parm.value.each { |p| expect(p).to be >= parm.allowed_range.first }
        parm.value.each { |p| expect(p).to be <= parm.allowed_range.last }
      end
      idx += 1
    end
  end

  it 'has a :valid? method that works' do
    # when valid
    pg = Rakr::Common::ParameterGroup.new(@group_name, @fake_conf[@group_test_name], @good_chunk)
    (validity, msg) = pg.valid?
    expect(validity).to be true
    expect(msg.empty?).to be true
    # when invalid
    pg = Rakr::Common::ParameterGroup.new(@group_name, @fake_conf_invalid[@group_test_name], @bad_chunk)
    (validity, msg) = pg.valid?
    expect(validity).not_to be true
    expect(msg).to eq("Rakr::Common::UnsignedParameter: \"test unsigned\" value: #{@unsigned_data_should_be[0]} (allowed_range: [0, 1])\nRakr::Common::SignedParameter: \"test signed\" value: #{@signed_data_should_be[1]} (allowed_range: [-128, -127])\nRakr::Common::UnsignedParameter: \"test unsigned 4\" value: #{@unsigned_data_should_be} (allowed_range: [0, 1])\nRakr::Common::SignedParameter: \"test signed 4\" value: #{@signed_data_should_be} (allowed_range: [-128, -127])\n")
  end

  it 'has a :create_pic_drawing method that works' do
    d = Rakr::Pic::Drawing.new(@good_akp_file, Rakr::AKP::File)
    pg = Rakr::Common::ParameterGroup.new(@group_name, @fake_conf[@group_test_name], @good_chunk)
    expect(pg.create_pic_drawing(d, 0, 0, @group_test_name).empty?).not_to be true
    expect(pg.create_pic_drawing(d, 0, 0, @group_test_name)).to match(/PGtest0000: box invis wid .*$/)
    expect(pg.create_pic_drawing(d, 1, 1, @group_test_name)).to match(/PGtest0101: box invis wid .*$/)
  end

  it 'has a :lookup method that works' do
    pg = Rakr::Common::ParameterGroup.new(@group_name, @fake_conf[@group_test_name], @good_chunk)
    expect(pg.respond_to?(:lookup)).to be true
    @fake_conf[@group_test_name].keys.each do
      |pname|
      expect((par = pg.lookup(pname))).not_to be nil
      expect(par.name).to(eq(pname), "#{par.name} != #{pname}")
    end
  end

end
