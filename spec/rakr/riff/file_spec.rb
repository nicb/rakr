# frozen_string_literal: true

RSpec.describe Rakr::Riff::File do

  before :example do
    # a file with 2 chunks
    @good_riff_file = File.join(FIXTURES_PATH, 'riff_test.riff')
    #
    # a bad riff file which can be read skipping the first 12 bytes
    #
    @good_akp_file = File.join(FIXTURES_PATH, 'akp_test.akp')
    @n_akp_chunks = 31
  end
  
  it 'can be created' do
    expect(Rakr::Riff::File.new(@good_riff_file)).not_to be nil
    expect(Rakr::Riff::File.new(@good_akp_file, 12)).not_to be nil
  end

  it 'has one or more Chunk objects' do
    riff = Rakr::Riff::File.new(@good_riff_file)
    expect(riff.chunks.size).to be > 0
    expect(riff.chunks.size).to eq(2)
    #
    akp = Rakr::Riff::File.new(@good_akp_file, 12)
    expect(akp.chunks.size).to be > 0
    expect(akp.chunks.size).to eq(@n_akp_chunks)
  end

  it 'has non-empty data' do
    riff = Rakr::Riff::File.new(@good_riff_file)
    expect(riff.chunks.first.data.empty?).to be true
    expect(riff.chunks[1].data.empty?).not_to be true
    #
    akp = Rakr::Riff::File.new(@good_akp_file, 12)
    akp.chunks.each { |c| expect(c.data.empty?).not_to be true }
  end

end
