# frozen_string_literal: true

RSpec.describe Rakr::AKP::KeyGroup do
  
  before :example do
    @good_akp_file = File.join(FIXTURES_PATH, 'akp_test.akp')
    @akp_file = Rakr::AKP::File.new(@good_akp_file)
    @conf = YAML.load(File.open(Rakr::AKP::AKP_FORMAT_SPEC, 'r'), __FILE__)
    @group_name = 'kgrp'
    @kgrp_chunk = @akp_file.lookup(@group_name)
    @conf = @conf[@group_name]
    @n_parameters_should_be = 102
    @parameter_names = ['low_note', 'high_note', 'sample_name']
    @note_names = [[21, "Am1"], [24, "Cp0"], [60, "Cp3"], [61, "CSp3"], [67, "Gp3"], [72, "Cp4"], [84, "Cp5"]]
  end
  
  it 'can be created' do
    expect(Rakr::AKP::KeyGroup.new(@group_name, @conf, @kgrp_chunk)).not_to be nil
  end

  it 'can be created correctly' do
    pg = Rakr::AKP::KeyGroup.new(@group_name, @conf, @kgrp_chunk)
    expect(pg.parameters.empty?).not_to be true
    expect(pg.parameters.size).to eq(@n_parameters_should_be)
    pg.parameters.each { |parm| expect(parm.kind_of?(Rakr::Common::Parameter)).to be true }
    idx = 0
    pg.parameters.each do
      |parm|
      expect(parm.valid?).to be true
      unless parm.kind_of?(Rakr::Common::StringParameter)
        expect(parm.value).to be >= parm.allowed_range.first
        expect(parm.value).to be <= parm.allowed_range.last
      end
      idx += 1
    end
  end

  it 'has a :valid? method that works' do
    # when valid
    pg = Rakr::AKP::KeyGroup.new(@group_name, @conf, @kgrp_chunk)
    (validity, msg) = pg.valid?
    expect(validity).to be true
    expect(msg.empty?).to be true
  end

  it 'has a :lookup method that works' do
    kg = Rakr::AKP::KeyGroup.new(@group_name, @conf, @kgrp_chunk)
    expect(kg.respond_to?(:lookup)).to be true
    @parameter_names.each do
      |pname|
      expect((par = kg.lookup(pname))).not_to be nil
      expect(par.name).to(eq(pname), "#{par.name} != #{pname}")
    end
  end

  it 'has a private :midi2note method that works' do
    kg = Rakr::AKP::KeyGroup.new(@group_name, @conf, @kgrp_chunk)
    @note_names.each { |num, should_be| expect((res = kg.send(:m2n, num))).to(eq(should_be), "#{res} != #{should_be}") }
  end

end
