# frozen_string_literal: true

RSpec.describe Rakr::AKP::File do
  
  before :example do
    @good_akp_file = File.join(FIXTURES_PATH, 'akp_test.akp')
    @bad_akp_file = File.join(FIXTURES_PATH, 'bad_akp_file.akp')
  end
  
  it 'can be created' do
    expect(Rakr::AKP::File.new(@good_akp_file)).not_to be nil
  end

  it 'has one or more Chunk objects' do
    akp = Rakr::AKP::File.new(@good_akp_file)
    expect(akp.chunks.size).to be > 0
  end

  it 'has non-empty data' do
    akp = Rakr::AKP::File.new(@good_akp_file)
    akp.chunks.each { |c| expect(c.data.empty?).not_to be true }
  end

  it "won't load invalid files" do
    expect { Rakr::AKP::File.new(@bad_akp_file) }.to raise_error(Rakr::InvalidParameterGroup)
  end

  it 'has all the expected chunks in the appropriate quantities' do
    akp = Rakr::AKP::File.new(@good_akp_file)
    expected_chunks = { 'prg ' => 1, 'out ' => 1, 'tune' => 1, 'lfo ' => 2, 'mods' => 1, 'kgrp' => akp.number_of_keygroups }
    expected_chunks.each do
      |name, qt|
      expect(akp.parameter_groups.has_key?(name)).to(be(true), name)
      expect(akp.parameter_groups[name].size).to eq(qt)
    end
  end

end
