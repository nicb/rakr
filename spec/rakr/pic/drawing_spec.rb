# frozen_string_literal: true

RSpec.describe Rakr::Pic::Drawing do
  
  before :example do
    @riff_file = File.join(FIXTURES_PATH, 'riff_test.riff')
    @akp_file = File.join(FIXTURES_PATH, 'akp_test.akp')
    @methods = [:output]
  end
  
  it 'can be created' do
    expect(Rakr::Pic::Drawing.new(@riff_file)).not_to be nil
  end

  it 'has methods to create the drawing' do
    d = Rakr::Pic::Drawing.new(@akp_file, Rakr::AKP::File)
    @methods.each { |m| expect(d.respond_to?(m)).to be true }
  end

  it 'raises a PureVirtualMethodCalled error when called on a basic RIFF file' do
    d = Rakr::Pic::Drawing.new(@riff_file)
    expect { d.output }.to raise_error(Rakr::PureVirtualMethodCalled)
  end

  it 'has an :output method which works properly' do
    d = Rakr::Pic::Drawing.new(@akp_file, Rakr::AKP::File)
    expect(d.output.empty?).not_to be true
  end

end
