# frozen_string_literal: true

RSpec.describe 'Rakr::Pic::Template.read' do
  
  before :example do
    @akp_file = File.join(FIXTURES_PATH, 'akp_test.akp')
    @drawing = Rakr::Pic::Drawing.new(@akp_file, Rakr::AKP::File)
    @methods = [ :header, :trailer ]
    @header_should_be = get_header
    @trailer_should_be = get_trailer
    @results = [ @header_should_be, @trailer_should_be ]
  end
  
  it 'does work' do
    #
    # header
    #
    expect(@drawing.send(:header).empty?).not_to be true
    expect(@drawing.send(:header)).to match(/\.po\s+\d+/)
    expect(@drawing.send(:header)).to match(/\.ll\s+\d+/)
    expect(@drawing.send(:header)).to match(/\.in\s+\d+/)
    expect(@drawing.send(:header)).to match(/\.PS\s+\d+/)
    expect(@drawing.send(:header)).to match(/OuterFrame:/)
    #
    # trailer
    #
    expect(@drawing.send(:trailer).empty?).not_to be true
    expect(@drawing.send(:trailer)).to eq(@trailer_should_be)
  end

end

def get_header
  res =<<-EOF
.po \\d+
.in 0.0i
.\\" The following numbers are empirically derived
.ll 16.141732283464567i
.PS 15.944881889763778i
scale=2.54;  # we run everything in centimeters here (not true but whatever)
maxpswid=maxpswid-2;
maxpsht=maxpsht-6.5;
OuterFrame: box invis wid maxpswid ht maxpsht
"\\s[+10]\\fBFile: akp_test.akp\\fP\\s0" with .s at OuterFrame.n + (0, 0.1)
#
# end of header.pic.erb
#
EOF
  res
end

def get_trailer
  res = <<-EOF
#
#
.PE
.\\"
.\\" end of trailer.pic.erb
.\\"
EOF
  res
end
